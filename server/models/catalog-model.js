const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Catalog = new Schema(
    {
        proID: { type: Number, required: true },
        check: { type: [String], required: true },
        Image: { type:  String, required: true },
        PName: { type: String, required: true },
        model: { type: String, required: true },
        Price: { type: Number, required: true },  
        Quantity: { type: Number, required: true },
        Stockstatus: { type: String, required: true },
        Status: { type: String, required: true },
        Action: { type: String, required: true },
        EnableFeed: { type: String, required: true },
        isChecked: { type: String, required: true },

    },

    { timestamps: true },
)

module.exports = mongoose.model('catalog', Catalog)
