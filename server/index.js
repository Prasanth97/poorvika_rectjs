const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const db = require('./db')
const catalogRouter = require('./routes/catalog-router')
//const catalogRouter1 = require('./routes/catalog-routers')


const app = express()
const apiPort = 3000

app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())
app.use(bodyParser.json())

db.on('error', console.error.bind(console, 'MongoDB connection error:'))

app.get('/', (req, res) => {
    res.send('database connected sucessfully')
})

app.use('/api', catalogRouter)

//app.use('/app', catalogRouter1)


app.listen(apiPort, () => console.log(`Server running on port ${apiPort}`))