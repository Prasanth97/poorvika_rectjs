const Catalog = require('../models/catalog-model')

createCatalog = (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a product detail',
        })
    }

    const catalog = new Catalog(body)

    if (!catalog) {
        return res.status(400).json({ success: false, error: err })
    }

    catalog
        .save()
        .then(() => {
            return res.status(201).json({
                success: true,
                id: catalog._id,
                message: 'catalog created!',
            })
        })
        .catch(error => {
            return res.status(400).json({
                error,
                message: 'catalog not created!',
            })
        })
}

updateCatalog = async (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a body to update',
        })
    }

    Catalog.findOne({ _id: req.params.id }, (err, movie) => {
        if (err) {
            return res.status(404).json({
                err,
                message: 'catalog not found!',
            })
        }
        catalog.proID = body.proID
        catalog.check = body.check
        catalog.Image = body.Image
        catalog.PName = body.PName
        catalog.model = body.model
        catalog.Price = body.Price
        catalog.Quantity = body.Quantity
        catalog.Stockstatus = body.Stockstatus
        catalog.Status = body.Status
        catalog.Action = body.Action
        catlog.EnableFeed = body.EnableFeed
        catalog.isChecked = body.isChecked

            .save()
            .then(() => {
                return res.status(200).json({
                    success: true,
                    id: catalog._id,
                    message: 'product updated!',
                })
            })
            .catch(error => {
                return res.status(404).json({
                    error,
                    message: 'product not updated!',
                })
            })
    })
}

deleteCatalog = async (req, res) => {
    await Catalog.findOneAndDelete({ _id: req.params.id }, (err, movie) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!catalog) {
            return res
                .status(404)
                .json({ success: false, error: `catalog not found` })
        }

        return res.status(200).json({ success: true, data: movie })
    }).catch(err => console.log(err))
}

getCatalogById = async (req, res) => {
    await Catalog.findOne({ _id: req.params.id }, (err, movie) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!catalog) {
            return res
                .status(404)
                .json({ success: false, error: `product not found` })
        }
        return res.status(200).json({ success: true, data: movie })
    }).catch(err => console.log(err))
}

getCatalogs = async (req, res) => {
    await Catalog.find({}, (err, catalogs) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!catalogs.length) {
            return res
                .status(404)
                .json({ success: false, error: `product not found` })
        }
        return res.status(200).json({ success: true, data: catalogs })
    }).catch(err => console.log(err))
}

module.exports = {
    createCatalog,
    updateCatalog,
    deleteCatalog,
    getCatalogs,
    getCatalogById,
}