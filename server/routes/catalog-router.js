const express = require('express')

const CatalogCtrl = require('../controllers/catalog-ctrl')

//const CatalogCtrl1 = require('../controllers/catalog-ctrls')


const router = express.Router()

router.post('/catalog', CatalogCtrl.createCatalog)
router.put('/catalog/:id', CatalogCtrl.updateCatalog)
router.delete('/catalog/:id', CatalogCtrl.deleteCatalog)
router.get('/catalog/:id', CatalogCtrl.getCatalogs)
router.get('/catalogs', CatalogCtrl.getCatalogById)


//router.post('/specification', CatalogCtrl1.createCatalog1)
//router.put('/catalog/:id', CatalogCtrl.updateCatalog)//
//router.delete('/catalog/:id', CatalogCtrl.deleteCatalog)
//router.get('/catalog/:id', CatalogCtrl.getCatalogs)
//router.get('/catalogs', CatalogCtrl.getCatalogById)


module.exports = router